package application;
import vehicles.Bicycle;
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] manyBicycles = new Bicycle[4];
        manyBicycles[0] = new Bicycle("manufacturerZ",5, 55.4);
        manyBicycles[1] = new Bicycle("manufacturerO",6, 65.2);
        manyBicycles[2] = new Bicycle("manufacturerT",7, 75.5);
        manyBicycles[3] = new Bicycle("manufacturerT2",8, 95.7);
        for(int i = 0; i < manyBicycles.length; i++) {
            System.out.println(manyBicycles[i]);
        }

    }
}
